import java.util.Date;

public class Block{

			private String hash;  //firma digitale espressa in hash attuale
			private String previoushash; //vecchia firma digitale espressa in hash
			private String dato; //messaggio
			private long time;  //tempo 
			private int nonce;
	
			public Block(String dato,String previoushash) throws Exception{
					dato=dato;
					previoushash=previoushash;
					time=new Date().getTime();
					hash=calcolaHash();
			}
			
			public String calcolaHash()  throws Exception{
				String calcola=StringUtil.applicaSha256(previoushash+Long.toString(time)+dato+Integer.toString(nonce));  //vecchiohash+data+dato
				return calcola;
			}
			public String getDato(){
				return dato;
			}
	
			public String getHash(){
				return hash;
			}
	
			public String getPreviousHash(){
				return previoushash;
			}
			
			public void mineBlock(int difficulty) throws Exception {
				String target = new String(new char[difficulty]).replace('\0', '0'); //Create a string with difficulty * "0" 
				while(!hash.substring( 0, difficulty).equals(target)) {
					nonce ++;
					hash = calcolaHash();
				}
				System.out.println("Block Mined!!! : " + hash);
			}
			



}