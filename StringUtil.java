//serve per generare l'hash

import java.security.MessageDigest;

public class StringUtil{
		
			public static String applicaSha256(String a) throws Exception{
			try{	
					//chiedo che sia sha256
					MessageDigest x=MessageDigest.getInstance("SHA-256");
					//Applico sha256 sulla stringa in input
					byte[] hash = x.digest(a.getBytes("UTF-8")); //standard unicode	        
					StringBuffer hexString = new StringBuffer(); //la stringa la faremo in esadecimale
					for (int i = 0; i < hash.length; i++) {
						String hex = Integer.toHexString(0xff & hash[i]); //lo genero in esadecimale
						if(hex.length() == 1) hexString.append('0'); //la prima stringa hash deve avere valore zero
							hexString.append(hex);
					}
					return hexString.toString();
				
				}catch(RuntimeException e){
					throw new RuntimeException(e);
				}
			}





}